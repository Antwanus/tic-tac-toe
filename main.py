# Tic-tac-toe, noughts and crosses, or Xs and Os is a paper-and-pencil game for two players who take turns marking
# the spaces in a three-by-three grid with X or O. The player who succeeds in placing three of their marks in a
# horizontal, vertical, or diagonal row is the winne
play_field = [' ' for _ in range(1, 10)]

def print_play_field():
    print(
        f'''
           {play_field[0]}\t|\t{play_field[1]}\t|\t{play_field[2]}
        --------+-------+-------
           {play_field[3]}\t|\t{play_field[4]}\t|\t{play_field[5]}
        --------+-------+-------
           {play_field[6]}\t|\t{play_field[7]}\t|\t{play_field[8]}'''
    )


print_play_field()

player_turn = 'X'
is_game_on = True
while is_game_on:
    # CHECK GAME OVER
    if ' ' not in play_field:
        print('\n\nGame Over\n\n')
        break
    # SWITCH BETWEEN 'X' and 'O'
    player_turn = 'X' if player_turn == 'O' else 'O'
    print(f'\n<<<   PLAYER {player_turn} is UP   >>>')

    input_is_unvalidated = True
    user_input = ''
    while input_is_unvalidated:
        user_input = input('''Make your move general:
             1  |  2  |  3 
            ----+-----+----
             4  |  5  |  6 
            ----+-----+---- 
             7  |  8  |  9 \n''')
        if int(user_input) in range(1, 10):
            input_is_unvalidated = False
        else:
            print(f'{user_input} is not in range 1 - 10')

    # print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    play_field[int(user_input)-1] = player_turn
    print_play_field()



